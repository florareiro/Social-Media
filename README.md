**DEPLOY NO VECEL**


## **INSTRUÇÕES PARA RODAR LOCALMENTE**

## **BACKEND**

###  1- no repositório backend instalar dependencias e pacotes node no terminal
##### cd backend
##### npm install



###  2- criar arquivo .env para compartimentar chaves de acesso
##### CLOUD_API_KEY=  
##### CLOUD_API_SECRET=
##### CLOUD_NAME=
##### JWT_LIFETIME=30d  
##### JWT_SECRET=  
##### MONGO_URI=
##### MODE=DEV


###  3-Iniciar Servidor no terminal
##### npm start


## **FRONTEND**

###  1- Abrir outro terminal node

###  2- instalar dependencias e pacotes
#### cd frontend
##### npm install

###  3 - Iniciar react-app em localhost:3000
##### npm start
